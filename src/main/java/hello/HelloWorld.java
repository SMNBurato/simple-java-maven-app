package hello;

import org.joda.time.LocalTime; // Utilizzando questa classe esterna devo inserire la dipendenza nel pom.xml

public class HelloWorld {
	public static void main(String[] args) {
		LocalTime currentTime = new LocalTime();
		System.out.println("The current local time is: " + currentTime);
		Greeter greeter = new Greeter();
		System.out.println(greeter.sayHello());	
	}
}
